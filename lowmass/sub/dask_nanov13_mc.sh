#!/bin/bash
#
#SBATCH --output=log2/%x_%j.out
#SBATCH --error=log2/%x_%j.err
#SBATCH --nodes=1
##SBATCH --ntasks=1
##SBATCH --mincpus=1
#SBATCH --mem-per-cpu=4G
##SBATCH --time=5:00:00          # means 1h 00m 00s
##SBATCH --partition long
##SBATCH --mem=0
## mail-type=BEGIN, END, FAIL, REQUEUE, ALL, STAGE_OUT, TIME_LIMIT_90
##SBATCH --mail-type=ALL
##SBATCH --mail-user=j.xiao@ip2i.in2p3.fr

echo -n "Hostname:              "
hostname -f
echo -n "Uptime:                "
uptime
echo
echo -n "Je suis:               "
id -a
echo

INIT_PATH=$PWD
echo $(date)" - [ Init path] "${PWD}
mkdir -p /scratch/jxiao/${SLURM_JOB_NAME}_${SLURM_JOB_NUM_NODES}
pushd /scratch/jxiao/${SLURM_JOB_NAME}_${SLURM_JOB_NUM_NODES}
cp -r ${INIT_PATH}/proc .
ls -al
echo "===========================================================> Run Cmd"
echo "python3 ${INIT_PATH}/../../scripts/run_analysis.py --dump ${INIT_PATH}/out2 --json-analysis ${INIT_PATH}/cfg/test_nanov13_${SLURM_JOB_NAME}.json --skipCQR --workers ${SLURM_JOB_CPUS_PER_NODE} --executor dask/ip2i --fiducialCuts none --doFlow_corrections --queue normal --memory 4GB --scaleout 400 --max-scaleout 600 --save ${INIT_PATH}/${SLURM_JOB_NAME}.coffea > ${INIT_PATH}/log2/${SLURM_JOB_NAME}.debug 2>&1"
echo "===========================================================< End Run"
python3 ${INIT_PATH}/../../scripts/run_analysis.py --dump ${INIT_PATH}/out2 --json-analysis ${INIT_PATH}/cfg/test_nanov13_${SLURM_JOB_NAME}.json --skipCQR --workers 1 --executor dask/ip2i --fiducialCuts none --doFlow_corrections --queue normal --memory 4GB --scaleout 400 --max-scaleout 600 --save ${INIT_PATH}/${SLURM_JOB_NAME}.coffea > ${INIT_PATH}/log2/${SLURM_JOB_NAME}.debug 2>&1
ls
