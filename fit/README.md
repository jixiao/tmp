# Install

Using `conda`, `mamba`, or `micromamba`, run the following to create the environment

```bash
micromamba env create -n dofit python zfit uproot mplhep hist -y
```

Then activate the environment

```bash
micromamba activate dofit
```