import awkward as ak
import coffea.util
import matplotlib.pyplot as plt
import mplhep as hep
import dask.dataframe as dd
import numpy as np

hep.cms.style.CMS["legend.handlelength"] = 1
hep.cms.style.CMS["legend.handleheight"] = 1
hep.cms.style.CMS["legend.frameon"] = False
hep.cms.style.CMS["axes.linewidth"] = 3
hep.style.use("CMS")

rlts = coffea.util.load(
    "/gridgroup/cms/jxiao/hgg/dl/lmtree/HiggsDNA/tests/hist_presel.coffea"
)

### Auxiliary function

import awkward as ak
import hist.dask as dah
import coffea.util
import matplotlib.pyplot as plt
import mplhep as hep
import dask.dataframe as dd
import dask.array as da
import numpy as np
from sklearn.impute import SimpleImputer
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import (
    OneHotEncoder,
    StandardScaler,
)
from sklearn.compose import ColumnTransformer
from sklearn.metrics import (
    accuracy_score,
    classification_report,
    confusion_matrix,
    roc_curve,
    auc,
)
import pandas as pd
import torch
import torch.nn as nn
import torch.optim as optim
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.utils.class_weight import compute_class_weight


import hist
import matplotlib.pyplot as plt
import mplhep as hep

plt.style.use(hep.style.CMS)  # or ATLAS/LHCb2


def load_data(input_path, is_signal=True, mass=95, rand_method="identity"):
    # drop columns
    ddf = dd.read_parquet(input_path)
    nevent = len(ddf)
    drop_list = [i for i in ddf.columns if i.endswith("IdxG")]
    ddf = ddf.drop(drop_list, axis=1)
    # add columns
    ddf["sample_type"] = 1 if is_signal else 0
    if isinstance(mass, list) and rand_method == "identity":
        ddf["mass_eigen"] = da.array(np.random.choice(mass, nevent))
    else:
        ddf["mass_eigen"] = mass
    ddf["PV_log_score"] = np.log(ddf["PV_score"])
    # scale pt selection & mass cut
    ddf = ddf[
        (ddf.mass > 55)
        & (ddf.lead_pt > 0.47 * ddf.mass)
        & (ddf.sublead_pt > 0.28 * ddf.mass)
    ]

    # eletron idx selection
    ddf = ddf[(ddf.lead_electronIdx < 0) & (ddf.sublead_electronIdx < 0)]

    # class label
    ddf["y"] = 1 if is_signal else 0
    # cat_cols=ddf.select_dtypes(exclude="number").columns
    # num_cols=ddf.select_dtypes(include="number").columns
    return ddf


def split_class(var_list, ddf):

    X_0, y = ddf[var_list], ddf["sample_type"]
    X, wgt, mass = (
        X_0.drop(["weight", "mass_eigen", "mass"], axis=1),
        X_0["weight"],
        X_0["mass_eigen"],
    )

    X_sig, wgt_sig, mass_sig = X[y == 1], wgt[y == 1], mass[y == 1]
    X_bkg, wgt_bkg, mass_bkg = X[y == 0], wgt[y == 0], mass[y == 0]
    return X, y, wgt, mass, X_sig, wgt_sig, mass_sig, X_bkg, wgt_bkg, mass_bkg


def get_distributions(
    vname, sig_arr, bkg_arr, sig_wgt, bkg_wgt, isbool=False, nbins=100
):
    # set plot range, keep 99.9% events
    if isbool:
        low_edge = -0.1
        high_edge = 1.1
    else:
        sig_range = np.percentile(sig_arr, [0.05, 99.95])
        bkg_range = np.percentile(bkg_arr, [0.05, 99.95])
        low_edge = sig_range[0] if sig_range[0] < bkg_range[0] else bkg_range[0]
        high_edge = sig_range[1] if sig_range[1] > bkg_range[1] else bkg_range[1]
        if not low_edge < high_edge:
            low_edge = low_edge - 0.1
            high_edge = high_edge + 0.1
    print(
        "variable: {}, low_edge: {}, high_edge: {}".format(vname, low_edge, high_edge)
    )
    hsig = hist.Hist(
        hist.axis.Regular(
            nbins, low_edge, high_edge, name=vname, label=vname, flow=None
        ),
        storage="weight",
        label="Counts",
    ).fill(sig_arr, weight=sig_wgt)
    hbkg = hist.Hist(
        hist.axis.Regular(
            nbins, low_edge, high_edge, name=vname, label=vname, flow=None
        ),
        storage="weight",
        label="Counts",
    ).fill(bkg_arr, weight=bkg_wgt)
    return hsig, hbkg


def make_comparison(X_sig, wgt_sig, X_bkg, wgt_bkg):
    # get columns
    cat_cols = list(X_sig.select_dtypes(exclude="number").columns)
    num_cols = list(X_sig.select_dtypes(include="number").columns)
    cat_cols.reverse()
    num_cols.reverse()

    row_of_plots = int(np.round((len(cat_cols) + len(num_cols)) / 2))
    fig, ax = plt.subplots(row_of_plots, 2, figsize=(20, 10 * (row_of_plots - 1)))

    for irow in range(0, row_of_plots):
        for icol in range(0, 2):
            if len(num_cols) > 0:
                vname = num_cols.pop()
                hsig, hbkg = get_distributions(
                    vname, X_sig[vname], X_bkg[vname], wgt_sig, wgt_bkg
                )
            elif len(cat_cols) > 0:
                vname = cat_cols.pop()
                hsig, hbkg = get_distributions(
                    vname,
                    X_sig[vname] * 1.0,
                    X_bkg[vname] * 1.0,
                    wgt_sig,
                    wgt_bkg,
                    isbool=True,
                )
            else:
                break
            (hsig / hsig.sum().value).plot(ax=ax[irow][icol], label="sig")
            (hbkg / hbkg.sum().value).plot(ax=ax[irow][icol], label="bkg")

            ax[irow][icol].legend()
            ax[irow][icol].grid(True)

    plt.style.use(hep.style.CMS)  # or ATLAS/LHCb2

    plt.savefig(f"all_plots/Norm_comparison_all.png", bbox_inches="tight")
    plt.savefig(f"all_plots/Norm_comparison_all.pdf", bbox_inches="tight")

    plt.close()
    return


def get_fitter(X, y, wgt, mass):

    # get columns
    cat_cols = list(X.select_dtypes(exclude="number").columns)
    num_cols = list(X.select_dtypes(include="number").columns)

    no_cat_cols = False
    if len(cat_cols) < 1:
        no_cat_cols = True
        print("No categorical columns")
    else:
        for i in cat_cols:
            X[i] = (X[i]).astype(str)

    categorical_pipline = Pipeline(
        steps=[
            ("impute", SimpleImputer(strategy="most_frequent")),
            ("oh-encode", OneHotEncoder(handle_unknown="ignore")),
        ]
    )

    numeric_pipeline = Pipeline(
        steps=[
            ("impute", SimpleImputer(strategy="mean")),
            ("scale", StandardScaler()),
        ]
    )

    if no_cat_cols:
        full_processor = ColumnTransformer(
            transformers=[("numeric", numeric_pipeline, list(num_cols))]
        )
    else:
        full_processor = ColumnTransformer(
            transformers=[
                ("numeric", numeric_pipeline, list(num_cols)),
                ("categorical", categorical_pipline, list(cat_cols)),
            ]
        )

    X_fit = full_processor.fit(X)
    y_fit = SimpleImputer(strategy="most_frequent").fit(y.values.reshape(-1, 1))
    wgt_fit = SimpleImputer(strategy="most_frequent").fit(wgt.values.reshape(-1, 1))
    mass_fit = SimpleImputer(strategy="most_frequent").fit(mass.values.reshape(-1, 1))

    return X_fit, y_fit, wgt_fit, mass_fit


def transformer(X_fit, y_fit, wgt_fit, mass_fit, X, y, wgt, mass):
    # numpy array
    X_np = X_fit.transform(X)
    y_np = y_fit.transform(y.values.reshape(-1, 1))
    wgt_np = wgt_fit.transform(wgt.values.reshape(-1, 1))
    mass_np = mass_fit.transform(mass.values.reshape(-1, 1))

    # dataframe
    X_df = pd.DataFrame(X_np, columns=X_fit.get_feature_names_out())
    y_df = pd.DataFrame(y_np, columns=["class"])
    wgt_df = pd.DataFrame(wgt_np, columns=["weight"])
    mass_df = pd.DataFrame(mass_np, columns=["mass"])

    return X_df, y_df, wgt_df, mass_df


# Step 6: Define your custom loss function with event-wise weights and class weights
# criterion = nn.BCELoss(weight=class_weights)
def weighted_binary_cross_entropy(outputs, targets, weights):
    # Binary cross-entropy loss
    bce_loss = nn.functional.binary_cross_entropy(outputs, targets, reduction="none")
    # Weighted loss
    weighted_loss = bce_loss * weights
    return weighted_loss.mean()


def save_onnx_model(model, input_size, conditioning_size, model_name):
    model.eval()
    # Define some dummy input and conditioning data for the model
    dummy_input = torch.randn((1, input_size), dtype=torch.float32)
    dummy_conditioning = torch.randn((1, conditioning_size), dtype=torch.float32)
    args = (dummy_input, dummy_conditioning)
    # Export the model to ONNX format
    onnx_program = torch.onnx.dynamo_export(
        model, *args, export_options=torch.onnx.ExportOptions(dynamic_shapes=True)
    ).save(model_name)

    return onnx_program


class EarlyStopping:
    def __init__(
        self, patience=7, verbose=False, delta=0, path="checkpoint.pt", trace_func=print
    ):
        """
        Args:
            patience (int): How long to wait after last time validation loss improved.
                            Default: 7
            verbose (bool): If True, prints a message for each validation loss improvement.
                            Default: False
            delta (float): Minimum change in the monitored quantity to qualify as an improvement.
                           Default: 0
            path (str): Path for the checkpoint to be saved to.
                        Default: 'checkpoint.pt'
            trace_func (function): trace print function.
                        Default: print
        """
        self.patience = patience
        self.verbose = verbose
        self.counter = 0
        self.best_score = None
        self.early_stop = False
        self.val_loss_min = np.Inf
        self.delta = delta
        self.path = path
        self.trace_func = trace_func

    def __call__(self, val_loss, model):

        score = -val_loss

        if self.best_score is None:
            self.best_score = score
            self.save_checkpoint(val_loss, model)
        elif score < self.best_score + self.delta:
            self.counter += 1
            if self.verbose:
                self.trace_func(
                    f"EarlyStopping counter: {self.counter} out of {self.patience}"
                )
            if self.counter >= self.patience:
                self.early_stop = True
        else:
            self.best_score = score
            self.save_checkpoint(val_loss, model)
            self.counter = 0

    def save_checkpoint(self, val_loss, model):
        """Saves model when validation loss decrease."""
        if self.verbose:
            self.trace_func(
                f"Validation loss decreased ({self.val_loss_min:.6f} --> {val_loss:.6f}).  Saving model ..."
            )
        torch.save(model.state_dict(), self.path)
        self.val_loss_min = val_loss


### Mass distrbutions

# create the axes
f, ax = plt.subplots(figsize=(10, 10))

hist_dy = rlts["h1d_diphoton_mass_v2"][{"dataset": "DYto2L_MLM_2022postEE"}]
(hist_dy / hist_dy.sum().value).plot(label="DY", color="black")
for i in [60, 65, 70, 75, 80, 85, 90, 95, 100, 105, 110]:
    hist_tmp = rlts["h1d_diphoton_mass_v2"][{"dataset": f"ggh_{i}_2022postEE"}]
    (hist_tmp / hist_tmp.sum().value).plot(label=i)

ax.set_xlim(50, 130)
ax.set_ylabel("A.U.")
hep.cms.label("Preliminary", loc=0, com=13.6, data=False, ax=ax)
plt.legend()
plt.style.use(hep.style.CMS)
plt.savefig(f"all_plots/mass_all.png", bbox_inches="tight")
plt.savefig(f"all_plots/mass_all.pdf", bbox_inches="tight")
plt.close()

hist_dy = rlts["h1d_diphoton_mass_v2"][{"dataset": "DYto2L_FXFX_2022postEE"}]
(hist_dy / hist_dy.sum().value).plot(label="DY", color="black")
for i in [65, 75, 85, 95, 105]:
    hist_tmp = rlts["h1d_diphoton_mass_v2"][{"dataset": f"ggh_{i}_2022postEE"}]
    (hist_tmp / hist_tmp.sum().value).plot(label=i)
plt.legend()
plt.style.use(hep.style.CMS)
plt.savefig(f"all_plots/mass_half_1.png", bbox_inches="tight")
plt.savefig(f"all_plots/mass_half_1.pdf", bbox_inches="tight")
plt.close()

hist_dy = rlts["h1d_diphoton_mass_v2"][{"dataset": "DYto2L_FXFX_2022postEE"}]
(hist_dy / hist_dy.sum().value).plot(label="DY", color="black")
for i in [60, 70, 80, 90, 100, 110]:
    hist_tmp = rlts["h1d_diphoton_mass_v2"][{"dataset": f"ggh_{i}_2022postEE"}]
    (hist_tmp / hist_tmp.sum().value).plot(label=i)
plt.legend()
plt.style.use(hep.style.CMS)
plt.savefig(f"all_plots/mass_half_2.png", bbox_inches="tight")
plt.savefig(f"all_plots/mass_half_2.pdf", bbox_inches="tight")
plt.close()

### Pre-process samples

#### Load data

ddf_m60 = load_data(
    "/gridgroup/cms/jxiao/hgg/dl/lmtree/HiggsDNA/tests/out_presel_postproc/merge/ggh_60_2022postEE/NOMINAL/UNTAGGED_merged.parquet",
    mass=60,
)
ddf_m70 = load_data(
    "/gridgroup/cms/jxiao/hgg/dl/lmtree/HiggsDNA/tests/out_presel_postproc/merge/ggh_70_2022postEE/NOMINAL/UNTAGGED_merged.parquet",
    mass=70,
)
ddf_m80 = load_data(
    "/gridgroup/cms/jxiao/hgg/dl/lmtree/HiggsDNA/tests/out_presel_postproc/merge/ggh_80_2022postEE/NOMINAL/UNTAGGED_merged.parquet",
    mass=80,
)
ddf_m90 = load_data(
    "/gridgroup/cms/jxiao/hgg/dl/lmtree/HiggsDNA/tests/out_presel_postproc/merge/ggh_90_2022postEE/NOMINAL/UNTAGGED_merged.parquet",
    mass=90,
)
ddf_m100 = load_data(
    "/gridgroup/cms/jxiao/hgg/dl/lmtree/HiggsDNA/tests/out_presel_postproc/merge/ggh_100_2022postEE/NOMINAL/UNTAGGED_merged.parquet",
    mass=100,
)
ddf_m110 = load_data(
    "/gridgroup/cms/jxiao/hgg/dl/lmtree/HiggsDNA/tests/out_presel_postproc/merge/ggh_110_2022postEE/NOMINAL/UNTAGGED_merged.parquet",
    mass=110,
)

ddf_m65 = load_data(
    "/gridgroup/cms/jxiao/hgg/dl/lmtree/HiggsDNA/tests/out_presel_postproc/merge/ggh_65_2022postEE/NOMINAL/UNTAGGED_merged.parquet",
    mass=65,
)
ddf_m75 = load_data(
    "/gridgroup/cms/jxiao/hgg/dl/lmtree/HiggsDNA/tests/out_presel_postproc/merge/ggh_75_2022postEE/NOMINAL/UNTAGGED_merged.parquet",
    mass=75,
)
ddf_m85 = load_data(
    "/gridgroup/cms/jxiao/hgg/dl/lmtree/HiggsDNA/tests/out_presel_postproc/merge/ggh_85_2022postEE/NOMINAL/UNTAGGED_merged.parquet",
    mass=85,
)
ddf_m95 = load_data(
    "/gridgroup/cms/jxiao/hgg/dl/lmtree/HiggsDNA/tests/out_presel_postproc/merge/ggh_95_2022postEE/NOMINAL/UNTAGGED_merged.parquet",
    mass=95,
)
ddf_m105 = load_data(
    "/gridgroup/cms/jxiao/hgg/dl/lmtree/HiggsDNA/tests/out_presel_postproc/merge/ggh_105_2022postEE/NOMINAL/UNTAGGED_merged.parquet",
    mass=105,
)

# ddf_bkg=load_data("out_presel_postproc/merge/DYto2L_FXFX_2022postEE/NOMINAL/UNTAGGED_merged.parquet",mass=[60,70,80,90,100,110],rand_method="identity")
ddf_bkg = load_data(
    "/gridgroup/cms/jxiao/hgg/dl/lmtree/HiggsDNA/tests/out_presel_postproc/merge/DYto2L_FXFX_2022postEE/NOMINAL/UNTAGGED_merged.parquet",
    mass=[65, 75, 85, 95, 105],
    rand_method="identity",
    is_signal=False,
)


# ddf = dd.concat([ddf_m60,ddf_m70,ddf_m80,ddf_m90,ddf_m100,ddf_m110,ddf_bkg])
ddf = dd.concat([ddf_m65, ddf_m75, ddf_m85, ddf_m95, ddf_m105, ddf_bkg])
ddf = ddf.compute()

ddf_bkg2 = load_data(
    "/gridgroup/cms/jxiao/hgg/dl/lmtree/HiggsDNA/tests/out_presel_postproc/merge/DYto2L_FXFX_2022postEE/NOMINAL/UNTAGGED_merged.parquet",
    mass=[60, 70, 80, 90, 100, 110],
    rand_method="identity",
    is_signal=False,
)
ddf2 = dd.concat([ddf_m60, ddf_m70, ddf_m80, ddf_m90, ddf_m100, ddf_m110, ddf_bkg2])
ddf2 = ddf2.compute()

### Make comparison plots

####  variable list


var_list = [
    "lead_eta",
    "lead_hoe",
    "lead_mvaID",
    "lead_phi",
    "lead_pt",
    "lead_r9",
    "lead_sieie",
    # "lead_electronIdx",
    # "lead_electronVeto",
    # "lead_seedGain",
    "sublead_eta",
    "sublead_hoe",
    "sublead_mvaID",
    "sublead_phi",
    "sublead_pt",
    "sublead_r9",
    "sublead_sieie",
    # "sublead_electronIdx",
    # "sublead_electronVeto",
    # "sublead_seedGain",
    "pt",
    "eta",
    "phi",
    "PV_ndof",
    "PV_x",
    "PV_y",
    "PV_z",
    "PV_chi2",
    # "PV_score",
    "PV_npvsGood",
    "PV_log_score",
    # "mass",
    "mass_eigen",
    "weight",
]


def split_class(var_list, ddf):

    X_0, y = ddf[var_list], ddf["sample_type"]
    X, wgt, mass = (
        X_0.drop(["weight", "mass_eigen"], axis=1),
        X_0["weight"],
        X_0["mass_eigen"],
    )

    X_sig, wgt_sig, mass_sig = X[y == 1], wgt[y == 1], mass[y == 1]
    X_bkg, wgt_bkg, mass_bkg = X[y == 0], wgt[y == 0], mass[y == 0]
    return X, y, wgt, mass, X_sig, wgt_sig, mass_sig, X_bkg, wgt_bkg, mass_bkg


X, y, wgt, mass, X_sig, wgt_sig, mass_sig, X_bkg, wgt_bkg, mass_bkg = split_class(
    var_list, ddf
)

X2, y2, wgt2, mass2, X_sig2, wgt_sig2, mass_sig2, X_bkg2, wgt_bkg2, mass_bkg2 = (
    split_class(var_list, ddf2)
)

##### Compare distributions


make_comparison(X_sig, wgt_sig, X_bkg, wgt_bkg)

#### Preprocess

##### get fitter


from sklearn.model_selection import train_test_split

# Define test size (portion for testing, typically 0.2 or 0.3)
test_size = 0.25
# Randomly split data into training and test sets (using NumPy with Jax arrays)
X_train0, X_test0, y_train0, y_test0, wgt_train0, wgt_test0, mass_train0, mass_test0 = (
    train_test_split(X, y, wgt, mass, test_size=test_size, random_state=42)
)

# in principle, only for training dataset
X_fit, y_fit, wgt_fit, mass_fit = get_fitter(
    X_train0, y_train0, wgt_train0, mass_train0
)

##### transform
X_train, y_train, wgt_train, mass_train = transformer(
    X_fit, y_fit, wgt_fit, mass_fit, X_train0, y_train0, wgt_train0, mass_train0
)
X_test, y_test, wgt_test, mass_test = transformer(
    X_fit, y_fit, wgt_fit, mass_fit, X_test0, y_test0, wgt_test0, mass_test0
)

(
    X2_train0,
    X2_test0,
    y2_train0,
    y2_test0,
    wgt2_train0,
    wgt2_test0,
    mass2_train0,
    mass2_test0,
) = train_test_split(X2, y2, wgt2, mass2, test_size=0.9, random_state=42)

X2_test, y2_test, wgt2_test, mass2_test = transformer(
    X_fit, y_fit, wgt_fit, mass_fit, X2_test0, y2_test0, wgt2_test0, mass2_test0
)

import pickle

pickle.dump(
    {"X": X_fit, "y": y_fit, "weight": wgt_fit, "mass": mass_fit},
    open("model_file/scaler.pkl", "wb"),
)

#### correlation matrix
import seaborn as sns

f, ax = plt.subplots(figsize=(20, 16))
corr_sig = (X_train[y_train["class"] == 1]).corr()
sns.heatmap(
    corr_sig,
    mask=np.zeros_like(corr_sig, dtype=bool),
    cmap=sns.diverging_palette(220, 10, as_cmap=True),
    square=True,
    ax=ax,
    label="Signal Corr. Matrix (Train)",
)
plt.savefig(f"all_plots/cor_matrix_sig_train.png", bbox_inches="tight")
plt.savefig(f"all_plots/cor_matrix_sig_train.pdf", bbox_inches="tight")
plt.close()


f, ax = plt.subplots(figsize=(20, 16))
corr_bkg = (X_train[y_train["class"] == 0]).corr()
sns.heatmap(
    corr_bkg,
    mask=np.zeros_like(corr_bkg, dtype=bool),
    cmap=sns.diverging_palette(220, 10, as_cmap=True),
    square=True,
    ax=ax,
    label="Background Corr. Matrix (Train)",
)

plt.savefig(f"all_plots/cor_matrix_bkg_train.png", bbox_inches="tight")
plt.savefig(f"all_plots/cor_matrix_bkg_train.pdf", bbox_inches="tight")
plt.close()

f, ax = plt.subplots(figsize=(20, 16))
corr_sig = (X_test[y_test["class"] == 1]).corr()
sns.heatmap(
    corr_sig,
    mask=np.zeros_like(corr_sig, dtype=bool),
    cmap=sns.diverging_palette(220, 10, as_cmap=True),
    square=True,
    ax=ax,
    label="Signal Corr. Matrix (Test)",
)
plt.savefig(f"all_plots/cor_matrix_sig_test.png", bbox_inches="tight")
plt.savefig(f"all_plots/cor_matrix_sig_test.pdf", bbox_inches="tight")
plt.close()


f, ax = plt.subplots(figsize=(20, 16))
corr_bkg = (X_test[y_test["class"] == 0]).corr()
sns.heatmap(
    corr_bkg,
    mask=np.zeros_like(corr_bkg, dtype=bool),
    cmap=sns.diverging_palette(220, 10, as_cmap=True),
    square=True,
    ax=ax,
    label="Background Corr. Matrix (Test)",
)

plt.savefig(f"all_plots/cor_matrix_bkg_test.png", bbox_inches="tight")
plt.savefig(f"all_plots/cor_matrix_bkg_test.pdf", bbox_inches="tight")
plt.close()

sum(mass_train["mass"] == 65), sum(mass_train["mass"] == 75), sum(
    mass_train["mass"] == 85
), sum(mass_train["mass"] == 95), sum(mass_train["mass"] == 105)


from sklearn.utils.class_weight import compute_class_weight
import torch.optim as optim

class_wgt_train = compute_class_weight(
    "balanced", classes=np.unique(y_train), y=y_train.to_numpy().flatten()
)
final_wgt_train = (
    (y_train < 0.5) * class_wgt_train[0] + (y_train > 0.5) * class_wgt_train[1]
).to_numpy() * np.sign(wgt_train)

class_wgt_test = compute_class_weight(
    "balanced", classes=np.unique(y_test), y=y_test.to_numpy().flatten()
)
final_wgt_test = (
    (y_test < 0.5) * class_wgt_test[0] + (y_test > 0.5) * class_wgt_test[1]
).to_numpy() * np.sign(wgt_test)

input_size = X_train.shape[1]
conditioning_size = 1  # Adjust this according to the number of additional variables

# model = BinaryClassifier(input_size, conditioning_size)

# optimizer = optim.Adam(model.parameters(), lr=0.001)


### Define Network
# Step 4: Define your model
class PNNClassifier(nn.Module):
    def __init__(
        self,
        input_size,
        conditioning_size,
        dropout_prob=0.25,
        weight_decay=0.01,
        bias_decay=0.01,
    ):
        super(PNNClassifier, self).__init__()
        self.conditioning_size = conditioning_size

        self.fc1 = nn.Linear(input_size, 64)
        self.affine_layers1 = nn.Linear(conditioning_size, 4)
        self.bn1 = nn.BatchNorm1d(64 + 4)  # Batch Normalization after fc1

        self.fc2 = nn.Linear(64 + 4, 64)
        self.affine_layers2 = nn.Linear(conditioning_size, 4)
        self.bn2 = nn.BatchNorm1d(64 + 4)  # Batch Normalization after fc1

        self.fc3 = nn.Linear(64 + 4, 64)
        self.affine_layers3 = nn.Linear(conditioning_size, 4)
        self.bn3 = nn.BatchNorm1d(64 + 4)  # Batch Normalization after fc1

        self.fc4 = nn.Linear(64 + 4, 1)
        self.sigmoid = nn.Sigmoid()

        self.dropout = nn.Dropout(dropout_prob)
        self.weight_decay = weight_decay
        self.bias_decay = bias_decay

    def forward(self, x, conditioning):
        # print("===> 0",x.shape)
        x = torch.relu(self.fc1(x))
        affine1 = torch.relu(self.affine_layers1(conditioning))
        x = torch.cat((x, affine1), dim=1)
        x = self.bn1(x)  # Batch Normalization after fc1
        x = self.dropout(x)

        x = torch.relu(self.fc2(x))
        affine2 = torch.relu(self.affine_layers2(conditioning))
        x = torch.cat((x, affine2), dim=1)
        x = self.bn2(x)  # Batch Normalization after fc1
        x = self.dropout(x)

        x = torch.relu(self.fc3(x))
        affine3 = torch.relu(self.affine_layers3(conditioning))
        x = torch.cat((x, affine3), dim=1)
        x = self.bn3(x)  # Batch Normalization after fc1
        x = self.dropout(x)

        x = self.sigmoid(self.fc4(x))
        return x

    def l2_regularization(self):
        weight_reg = 0
        bias_reg = 0
        for param_name, param in self.named_parameters():
            if "weight" in param_name:
                weight_reg += torch.linalg.vector_norm(param, ord=2)
            elif "bias" in param_name:
                bias_reg += torch.linalg.vector_norm(param, ord=2)
        return self.weight_decay * weight_reg + self.bias_decay * bias_reg


class NNClassifier(nn.Module):
    def __init__(
        self,
        input_size,
        dropout_prob=0.25,
        weight_decay=0.01,
        bias_decay=0.01,
    ):
        super(NNClassifier, self).__init__()

        self.fc1 = nn.Linear(input_size, 64)
        self.bn1 = nn.BatchNorm1d(64)  # Batch Normalization after fc1

        self.fc2 = nn.Linear(64, 32)
        self.bn2 = nn.BatchNorm1d(32)  # Batch Normalization after fc1

        self.fc3 = nn.Linear(32, 32)
        self.bn3 = nn.BatchNorm1d(32)  # Batch Normalization after fc1

        self.fc4 = nn.Linear(32, 1)
        self.sigmoid = nn.Sigmoid()

        self.dropout = nn.Dropout(dropout_prob)
        self.weight_decay = weight_decay
        self.bias_decay = bias_decay

    def forward(self, x):
        # print("===> 0",x.shape)
        x = torch.relu(self.fc1(x))
        x = self.bn1(x)  # Batch Normalization after fc1
        x = self.dropout(x)

        x = torch.relu(self.fc2(x))
        x = self.bn2(x)  # Batch Normalization after fc1
        x = self.dropout(x)

        x = torch.relu(self.fc3(x))
        x = self.bn3(x)  # Batch Normalization after fc1
        x = self.dropout(x)

        x = self.sigmoid(self.fc4(x))
        return x

    def l2_regularization(self):
        weight_reg = 0
        bias_reg = 0
        for param_name, param in self.named_parameters():
            if "weight" in param_name:
                weight_reg += torch.linalg.vector_norm(param, ord=2)
            elif "bias" in param_name:
                bias_reg += torch.linalg.vector_norm(param, ord=2)
        return self.weight_decay * weight_reg + self.bias_decay * bias_reg


#### compare roc
def compare_roc(
    prefix=None,
    model_path=None,
    ninput=24,
    ncondition=None,
    mass=None,
    X_test=None,
    y_test=None,
    mass_test=None,
    X_train=None,
    y_train=None,
    mass_train=None,
):
    from sklearn.metrics import confusion_matrix, roc_curve, auc

    if not ncondition == None:
        model = PNNClassifier(ninput, ncondition)
    else:
        model = NNClassifier(ninput)
    model.load_state_dict(torch.load(model_path))
    model.eval()

    if not ncondition == None:
        if not mass == None:
            X_test_0, mass_test_0, y_test_0 = (
                X_test[mass_test.mass == mass],
                mass_test[mass_test.mass == mass],
                y_test[mass_test.mass == mass],
            )
        else:
            X_test_0, mass_test_0, y_test_0 = (X_test, mass_test, y_test)

        # Step 2: Plot ROC curve
        with torch.no_grad():
            inputs = torch.tensor(X_test_0.to_numpy(), dtype=torch.float32)
            conditioning = torch.tensor(
                mass_test_0.to_numpy(), dtype=torch.float32
            )  # Assuming 'additional_variables_test' is the DataFrame containing additional variables for testing
            outputs = model(inputs, conditioning).numpy()

        fpr, tpr, thresholds = roc_curve(y_test_0, outputs)
        roc_auc = auc(fpr, tpr)

        plt.figure(figsize=(10, 10))
        plt.plot(
            fpr, tpr, color="darkorange", lw=2, label=f"Test (AUC = {roc_auc:.4f})"
        )
        plt.plot([0, 1], [0, 1], color="navy", lw=2, linestyle="--")
        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.05])
        plt.xlabel("Background Eff.")
        plt.ylabel("Signal Eff.")
        plt.title("ROC Curve")
        # plt.legend(loc="lower right")
        # plt.show()

        if isinstance(X_train, pd.core.frame.DataFrame):
            if not mass == None:
                X_train_0, mass_train_0, y_train_0 = (
                    X_train[mass_train.mass == mass],
                    mass_train[mass_train.mass == mass],
                    y_train[mass_train.mass == mass],
                )
            else:
                X_train_0, mass_train_0, y_train_0 = (X_train, mass_train, y_train)

            # Step 2: Plot ROC curve
            with torch.no_grad():
                inputs = torch.tensor(X_train_0.to_numpy(), dtype=torch.float32)
                conditioning = torch.tensor(
                    mass_train_0.to_numpy(), dtype=torch.float32
                )  # Assuming 'additional_variables_test' is the DataFrame containing additional variables for testing
                outputs = model(inputs, conditioning).numpy()

            fpr, tpr, thresholds = roc_curve(y_train_0, outputs)
            roc_auc = auc(fpr, tpr)

            # plt.figure()
            plt.plot(
                fpr, tpr, color="green", lw=2, label=f"Train (AUC = {roc_auc:.4f})"
            )
        print(f"{prefix}_roc_M{mass} ---> Train (AUC = {roc_auc})")
        plt.legend(loc="lower right")
        plt.show()
    else:
        if not mass == None:
            X_test_0, mass_test_0, y_test_0 = (
                X_test[mass_test.mass == mass],
                mass_test[mass_test.mass == mass],
                y_test[mass_test.mass == mass],
            )
        else:
            X_test_0, mass_test_0, y_test_0 = (X_test, mass_test, y_test)

        # Step 2: Plot ROC curve
        with torch.no_grad():
            inputs = torch.tensor(X_test_0.to_numpy(), dtype=torch.float32)
            outputs = model(inputs).numpy()

        fpr, tpr, thresholds = roc_curve(y_test_0, outputs)
        roc_auc = auc(fpr, tpr)

        plt.figure(figsize=(10, 10))
        plt.plot(
            fpr, tpr, color="darkorange", lw=2, label=f"Test (AUC = {roc_auc:.4f})"
        )
        print(f"{prefix}_roc_M{mass} ---> Test (AUC = {roc_auc})")
        plt.plot([0, 1], [0, 1], color="navy", lw=2, linestyle="--")
        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.05])
        plt.xlabel("Background Eff.")
        plt.ylabel("Signal Eff.")
        plt.title("ROC Curve")
        # plt.legend(loc="lower right")
        # plt.show()

        if isinstance(X_train, pd.core.frame.DataFrame):
            if not mass == None:
                X_train_0, mass_train_0, y_train_0 = (
                    X_train[mass_train.mass == mass],
                    mass_train[mass_train.mass == mass],
                    y_train[mass_train.mass == mass],
                )
            else:
                X_train_0, mass_train_0, y_train_0 = (X_train, mass_train, y_train)

            # Step 2: Plot ROC curve
            with torch.no_grad():
                inputs = torch.tensor(X_train_0.to_numpy(), dtype=torch.float32)
                outputs = model(inputs).numpy()

            fpr, tpr, thresholds = roc_curve(y_train_0, outputs)
            roc_auc = auc(fpr, tpr)

            # plt.figure()
            plt.plot(
                fpr, tpr, color="green", lw=2, label=f"Train (AUC = {roc_auc:.4f})"
            )
        plt.legend(loc="lower right")
        plt.show()

    plt.savefig(f"all_plots/{prefix}_roc_M{mass}.png", bbox_inches="tight")
    plt.savefig(f"all_plots/{prefix}_roc_M{mass}.pdf", bbox_inches="tight")
    plt.close()


#### compare socre
def compare_score(
    prefix=None,
    model_path=None,
    ninput=24,
    ncondition=None,
    mass=None,
    X_test=None,
    y_test=None,
    mass_test=None,
    X_train=None,
    y_train=None,
    mass_train=None,
):
    import hist
    from hist import Hist
    import matplotlib.pyplot as plt

    if not ncondition == None:
        model = PNNClassifier(ninput, ncondition)
    else:
        model = NNClassifier(ninput)
    model.load_state_dict(torch.load(model_path))
    model.eval()

    plt.figure(figsize=(10, 10))

    if not ncondition == None:
        if not mass == None:
            X_test_0, mass_test_0, y_test_0 = (
                X_test[mass_test.mass == mass],
                mass_test[mass_test.mass == mass],
                y_test[mass_test.mass == mass],
            )
        else:
            X_test_0, mass_test_0, y_test_0 = (X_test, mass_test, y_test)

        with torch.no_grad():
            inputs = torch.tensor(X_test_0.to_numpy(), dtype=torch.float32)
            conditioning = torch.tensor(
                mass_test_0.to_numpy(), dtype=torch.float32
            )  # Assuming you have additional variables
            outputs = model(inputs, conditioning).numpy()

        # make plots without weights
        hsig_test = Hist.new.Regular(100, 0, 1, name="Score").Double()
        hsig_test.fill(ak.flatten(outputs[y_test_0["class"] == 1]))
        hbkg_test = Hist.new.Regular(100, 0, 1, name="Score").Double()
        hbkg_test.fill(ak.flatten(outputs[y_test_0["class"] == 0]))

        fig, ax = plt.subplots(figsize=(10, 10))
        (hsig_test / hsig_test.sum()).plot(
            label=r"ggH$(\gamma\gamma)$ (test)", color="red", linestyle="-", ax=ax
        )
        (hbkg_test / hbkg_test.sum()).plot(
            label="DY (test)", color="blue", linestyle="-", ax=ax
        )

        if isinstance(X_train, pd.core.frame.DataFrame):
            if not mass == None:
                X_train_0, mass_train_0, y_train_0 = (
                    X_train[mass_train.mass == mass],
                    mass_train[mass_train.mass == mass],
                    y_train[mass_train.mass == mass],
                )
            else:
                X_train_0, mass_train_0, y_train_0 = (X_train, mass_train, y_train)

            with torch.no_grad():
                inputs = torch.tensor(X_train_0.to_numpy(), dtype=torch.float32)
                conditioning = torch.tensor(
                    mass_train_0.to_numpy(), dtype=torch.float32
                )  # Assuming you have additional variables
                outputs = model(inputs, conditioning).numpy()

            # make plots without weights
            hsig_train = Hist.new.Regular(100, 0, 1, name="Score").Double()
            hsig_train.fill(ak.flatten(outputs[y_train_0["class"] == 1]))
            hbkg_train = Hist.new.Regular(100, 0, 1, name="Score").Double()
            hbkg_train.fill(ak.flatten(outputs[y_train_0["class"] == 0]))

            (hsig_train / hsig_train.sum()).plot(
                label=r"ggH$(\gamma\gamma)$ (test)", color="red", linestyle="--", ax=ax
            )
            (hbkg_train / hbkg_train.sum()).plot(
                label="DY (test)", color="blue", linestyle="--", ax=ax
            )
    else:
        if not mass == None:
            X_test_0, mass_test_0, y_test_0 = (
                X_test[mass_test.mass == mass],
                mass_test[mass_test.mass == mass],
                y_test[mass_test.mass == mass],
            )
        else:
            X_test_0, mass_test_0, y_test_0 = (X_test, mass_test, y_test)

        with torch.no_grad():
            inputs = torch.tensor(X_test_0.to_numpy(), dtype=torch.float32)
            outputs = model(inputs).numpy()

        # make plots without weights
        hsig_test = Hist.new.Regular(100, 0, 1, name="Score").Double()
        hsig_test.fill(ak.flatten(outputs[y_test_0["class"] == 1]))
        hbkg_test = Hist.new.Regular(100, 0, 1, name="Score").Double()
        hbkg_test.fill(ak.flatten(outputs[y_test_0["class"] == 0]))

        fig, ax = plt.subplots(figsize=(10, 10))
        (hsig_test / hsig_test.sum()).plot(
            label=r"ggH$(\gamma\gamma)$ (test)", color="red", linestyle="-", ax=ax
        )
        (hbkg_test / hbkg_test.sum()).plot(
            label="DY (test)", color="blue", linestyle="-", ax=ax
        )

        if isinstance(X_train, pd.core.frame.DataFrame):
            if not mass == None:
                X_train_0, mass_train_0, y_train_0 = (
                    X_train[mass_train.mass == mass],
                    mass_train[mass_train.mass == mass],
                    y_train[mass_train.mass == mass],
                )
            else:
                X_train_0, mass_train_0, y_train_0 = (X_train, mass_train, y_train)

            with torch.no_grad():
                inputs = torch.tensor(X_train_0.to_numpy(), dtype=torch.float32)
                outputs = model(inputs).numpy()

            # make plots without weights
            hsig_train = Hist.new.Regular(100, 0, 1, name="Score").Double()
            hsig_train.fill(ak.flatten(outputs[y_train_0["class"] == 1]))
            hbkg_train = Hist.new.Regular(100, 0, 1, name="Score").Double()
            hbkg_train.fill(ak.flatten(outputs[y_train_0["class"] == 0]))

            (hsig_train / hsig_train.sum()).plot(
                label=r"ggH$(\gamma\gamma)$ (test)", color="red", linestyle="--", ax=ax
            )
            (hbkg_train / hbkg_train.sum()).plot(
                label="DY (test)", color="blue", linestyle="--", ax=ax
            )
    plt.xlabel("Output Score")
    plt.ylabel("Frequency")
    # plt.title("Output Score Distribution")
    plt.legend()
    plt.show()
    plt.savefig(f"all_plots/{prefix}_score_M{mass}.png", bbox_inches="tight")
    plt.savefig(f"all_plots/{prefix}_score_M{mass}.pdf", bbox_inches="tight")
    plt.close()


### Build PNN
import pandas as pd
import torch
import torch.nn as nn
import torch.optim as optim
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.utils.class_weight import compute_class_weight


def train_pnn(
    model=None,
    early_stopping=None,
    num_epochs=10,
    batch_size=64,
    model_name="pnn_epoch10",
    _input_size=None,
    _conditioning_size=None,
    _X_train=None,
    _mass_train=None,
    _y_train=None,
    _final_wgt_train=None,
    _X_test=None,
    _mass_test=None,
    _y_test=None,
    _final_wgt_test=None,
):
    optimizer = optim.Adam(model.parameters(), lr=0.001)
    # Learning rate scheduler and early stopping parameters
    # early_stopping = EarlyStopping(patience=10, verbose=True,path="checkpoint.pt")
    scheduler = optim.lr_scheduler.ReduceLROnPlateau(
        optimizer, mode="min", factor=0.1, patience=5, verbose=True
    )

    best_val_loss = float("inf")
    best_model_state = None
    best_epoch = 0

    # Lists to store losses
    train_losses = []
    val_losses = []
    train_accuracies = []
    val_accuracies = []

    for epoch in range(num_epochs):
        model.train()
        total_loss_train = 0.0
        correct_train = 0
        total_train = 0

        for i in range(0, len(_X_train), batch_size):
            if i + batch_size > len(_X_train):
                continue
            inputs = torch.tensor(
                _X_train[i : i + batch_size].to_numpy(), dtype=torch.float32
            )
            conditioning = torch.tensor(
                _mass_train[i : i + batch_size].to_numpy(), dtype=torch.float32
            ).view(-1, 1)
            targets = torch.tensor(
                _y_train[i : i + batch_size].to_numpy(), dtype=torch.float32
            ).view(-1, 1)
            weights = torch.tensor(
                _final_wgt_train[i : i + batch_size].to_numpy(), dtype=torch.float32
            )  # Event-wise weights

            optimizer.zero_grad()
            outputs = model(inputs, conditioning)
            loss = (
                weighted_binary_cross_entropy(outputs, targets, weights)
                + model.l2_regularization()
            )  # Add L2 regularization to the loss
            loss.backward()
            optimizer.step()

            total_loss_train += loss.item() * len(inputs)
            predicted = (outputs > 0.5).float()
            correct_train += (predicted == targets).sum().item()
            total_train += len(inputs)

        avg_loss_train = total_loss_train / total_train
        accuracy_train = correct_train / total_train
        # print(f'Epoch [{epoch+1}/{num_epochs}], Loss: {loss.item():.4f}')
        train_losses.append(avg_loss_train)
        train_accuracies.append(accuracy_train)

        # Validation
        model.eval()

        total_loss_val = 0.0
        correct_val = 0
        total_val = 0

        with torch.no_grad():
            for i in range(0, len(_X_test), batch_size):
                if i + batch_size > len(_X_test):
                    continue
                inputs = torch.tensor(
                    _X_test[i : i + batch_size].to_numpy(), dtype=torch.float32
                )
                # Assuming 'additional_variables_val' is the DataFrame containing additional variables for validation
                conditioning = torch.tensor(
                    _mass_test[i : i + batch_size].to_numpy(), dtype=torch.float32
                )
                targets = torch.tensor(
                    _y_test[i : i + batch_size].to_numpy(), dtype=torch.float32
                ).view(-1, 1)
                weights = torch.tensor(
                    _final_wgt_test[i : i + batch_size].to_numpy(), dtype=torch.float32
                )  # Event-wise weights

                outputs = model(inputs, conditioning)
                loss = (
                    weighted_binary_cross_entropy(outputs, targets, weights)
                    + model.l2_regularization()
                )  # Add L2 regularization to the loss

                total_loss_val += loss.item() * len(inputs)
                predicted = (outputs > 0.5).float()
                correct_val += (predicted == targets).sum().item()
                total_val += len(inputs)

        avg_loss_val = total_loss_val / total_val
        accuracy_val = correct_val / total_val
        val_losses.append(avg_loss_val)
        val_accuracies.append(accuracy_val)
        print(
            f"Epoch [{epoch+1}/{num_epochs}], Train Loss: {avg_loss_train:.4f}, Train Accuracy: {accuracy_train:.4f}, Val Loss: {avg_loss_val:.4f}, Val Accuracy: {accuracy_val:.4f}"
        )

        # Step the scheduler with the validation loss
        scheduler.step(avg_loss_val)

        # Save the model if validation loss is the lowest
        if avg_loss_val < best_val_loss:
            best_val_loss = avg_loss_val
            best_model_state = model.state_dict()
            best_epoch = epoch

        # early stopping
        early_stopping(avg_loss_val, model)
        if early_stopping.early_stop:
            print(f"[Early stop] at Epoch: {epoch}")
            break

    # Step 8: Load the best model
    if best_model_state is not None:
        model.load_state_dict(best_model_state)

    ### pytorch model
    torch.save(
        model.state_dict(),
        f"/gridgroup/cms/jxiao/hgg/dl/lmtree/HiggsDNA/tests/{model_name}.pth",
    )
    ### onnx model
    model.eval()
    # Define some dummy input and conditioning data for the model
    dummy_input = torch.randn((1, _input_size), dtype=torch.float32)
    dummy_conditioning = torch.randn((1, _conditioning_size), dtype=torch.float32)
    inputs = (dummy_input, dummy_conditioning)
    input_names = ["feature", "mass"]
    output_names = ["output"]
    # Export the model to ONNX format
    torch.onnx.export(
        model,
        inputs,
        f"/gridgroup/cms/jxiao/hgg/dl/lmtree/HiggsDNA/tests/{model_name}.onnx",
        export_params=True,
        input_names=input_names,
        output_names=output_names,
        dynamic_axes={
            "feature": {0: "batch_size"},
            "mass": {0: "batch_size"},
            "output": {0: "batch_size"},
        },
    )

    # Plotting the training and validation losses
    plt.figure(figsize=(10, 10))
    plt.plot(train_losses, label="Training Loss")
    plt.plot(val_losses, label="Validation Loss")
    plt.xlabel("Epochs")
    plt.ylabel("Loss")
    plt.legend()
    plt.title("Training and Validation Loss over Epochs")
    plt.savefig(f"all_plots/Loss_Epochs_{model_name}.png", bbox_inches="tight")
    plt.savefig(f"all_plots/Loss_Epochs_{model_name}.pdf", bbox_inches="tight")
    plt.close()

    # Plotting the training and validation losses
    plt.figure(figsize=(10, 10))
    plt.plot(train_accuracies, label="Training Accuracy")
    plt.plot(val_accuracies, label="Validation Accuracy")
    plt.xlabel("Epochs")
    plt.ylabel("Accuracy")
    plt.legend()
    plt.title("Training and Validation Accuracy over Epochs")
    plt.savefig(f"all_plots/Accuracy_Epochs_{model_name}.png", bbox_inches="tight")
    plt.savefig(f"all_plots/Accuracy_Epochs_{model_name}.pdf", bbox_inches="tight")
    plt.close()


train_pnn(
    model=PNNClassifier(input_size, conditioning_size, dropout_prob=0.25),
    early_stopping=EarlyStopping(patience=10, verbose=True, path="checkpoint_PNN.pt"),
    num_epochs=300,
    batch_size=128,
    model_name="pnn_epoch300",
    _input_size=input_size,
    _conditioning_size=conditioning_size,
    _X_train=X_train,
    _mass_train=mass_train,
    _y_train=y_train,
    _final_wgt_train=final_wgt_train,
    _X_test=X_test,
    _mass_test=mass_test,
    _y_test=y_test,
    _final_wgt_test=final_wgt_test,
)

# import shap

# model = PNNClassifier(input_size, conditioning_size)
# model.load_state_dict(
#     torch.load("/gridgroup/cms/jxiao/hgg/dl/lmtree/HiggsDNA/tests/pnn_epoch300.pth")
# )


# # model.eval()
# def predict(inputs):
#     # Preprocess X if needed (e.g., convert to tensors)
#     return model(
#         torch.tensor(inputs.drop("mass").to_numpy(), dtype=torch.float32),
#         torch.tensor(inputs["mass"].to_numpy(), dtype=torch.float32),
#     ).flatten()


# X_train_new = X_train
# X_train_new["mass"] = mass_train["mass"]
# X_test_new = X_test
# X_test_new["mass"] = mass_test["mass"]
# explainer = shap.Explainer(predict, X_train_new[:100])
# shap_values = explainer(X_test_new[:500])

# plt.figure(figsize=(10, 10))
# shap.plots.beeswarm(shap_values, max_display=20)
# plt.savefig(f"all_plots/shap_pnn_epoch300.png", bbox_inches="tight")
# plt.savefig(f"all_plots/shap_pnn_epoch300.pdf", bbox_inches="tight")
# plt.close()

for i in [None, 65, 75, 85, 95, 105]:
    print(f"===> mass {i}")
    compare_roc(
        "pnn_epoch300",
        "/gridgroup/cms/jxiao/hgg/dl/lmtree/HiggsDNA/tests/pnn_epoch300.pth",
        ninput=X_train.shape[1],
        ncondition=1,
        mass=i,
        X_test=X_test,
        y_test=y_test,
        mass_test=mass_test,
        X_train=X_train,
        y_train=y_train,
        mass_train=mass_train,
    )
    compare_score(
        "pnn_epoch300",
        "/gridgroup/cms/jxiao/hgg/dl/lmtree/HiggsDNA/tests/pnn_epoch300.pth",
        ninput=X_train.shape[1],
        ncondition=1,
        mass=i,
        X_test=X_test,
        y_test=y_test,
        mass_test=mass_test,
        X_train=X_train,
        y_train=y_train,
        mass_train=mass_train,
    )
for i in [60, 70, 80, 90, 100, 110]:
    print(f"===> mass {i}")
    compare_roc(
        "pnn_epoch300",
        "/gridgroup/cms/jxiao/hgg/dl/lmtree/HiggsDNA/tests/pnn_epoch300.pth",
        ninput=X_train.shape[1],
        ncondition=1,
        mass=i,
        X_test=X2_test,
        y_test=y2_test,
        mass_test=mass2_test,
    )
    compare_score(
        "pnn_epoch300",
        "/gridgroup/cms/jxiao/hgg/dl/lmtree/HiggsDNA/tests/pnn_epoch300.pth",
        ninput=X_train.shape[1],
        ncondition=1,
        mass=i,
        X_test=X2_test,
        y_test=y2_test,
        mass_test=mass2_test,
    )


### Build NN with mass

import pandas as pd
import torch
import torch.nn as nn
import torch.optim as optim
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.utils.class_weight import compute_class_weight


def train_nn(
    model=None,
    early_stopping=None,
    num_epochs=10,
    batch_size=64,
    model_name="pnn_epoch10",
    _input_size=None,
    _conditioning_size=None,
    _X_train=None,
    _mass_train=None,
    _y_train=None,
    _final_wgt_train=None,
    _X_test=None,
    _mass_test=None,
    _y_test=None,
    _final_wgt_test=None,
):
    optimizer = optim.Adam(model.parameters(), lr=0.001)
    # Learning rate scheduler and early stopping parameters
    # early_stopping = EarlyStopping(patience=10, verbose=True,path="checkpoint_PNN.pt"),
    scheduler = optim.lr_scheduler.ReduceLROnPlateau(
        optimizer, mode="min", factor=0.1, patience=5, verbose=True
    )

    best_val_loss = float("inf")
    best_model_state = None
    best_epoch = 0

    train_losses = []
    val_losses = []
    train_accuracies = []
    val_accuracies = []

    for epoch in range(num_epochs):
        model.train()
        total_loss_train = 0.0
        correct_train = 0
        total_train = 0

        for i in range(0, len(_X_train), batch_size):
            if i + batch_size > len(_X_train):
                continue
            inputs = torch.tensor(
                _X_train[i : i + batch_size].to_numpy(), dtype=torch.float32
            )
            targets = torch.tensor(
                _y_train[i : i + batch_size].to_numpy(), dtype=torch.float32
            ).view(-1, 1)
            weights = torch.tensor(
                _final_wgt_train[i : i + batch_size].to_numpy(), dtype=torch.float32
            )  # Event-wise weights

            optimizer.zero_grad()
            outputs = model(inputs)
            loss = (
                weighted_binary_cross_entropy(outputs, targets, weights)
                + model.l2_regularization()
            )  # Add L2 regularization to the loss
            loss.backward()
            optimizer.step()

            total_loss_train += loss.item() * len(inputs)
            predicted = (outputs > 0.5).float()
            correct_train += (predicted == targets).sum().item()
            total_train += len(inputs)

        avg_loss_train = total_loss_train / total_train
        accuracy_train = correct_train / total_train
        # print(f'Epoch [{epoch+1}/{num_epochs}], Loss: {loss.item():.4f}')
        train_losses.append(avg_loss_train)
        train_accuracies.append(accuracy_train)

        # Validation
        model.eval()
        total_loss_val = 0.0
        correct_val = 0
        total_val = 0

        with torch.no_grad():
            for i in range(0, len(_X_test), batch_size):
                if i + batch_size > len(_X_test):
                    continue
                inputs = torch.tensor(
                    _X_test[i : i + batch_size].to_numpy(), dtype=torch.float32
                )
                targets = torch.tensor(
                    _y_test[i : i + batch_size].to_numpy(), dtype=torch.float32
                ).view(-1, 1)
                weights = torch.tensor(
                    _final_wgt_test[i : i + batch_size].to_numpy(), dtype=torch.float32
                )  # Event-wise weights

                outputs = model(inputs)
                loss = (
                    weighted_binary_cross_entropy(outputs, targets, weights)
                    + model.l2_regularization()
                )  # Add L2 regularization to the loss

                total_loss_val += loss.item() * len(inputs)
                predicted = (outputs > 0.5).float()
                correct_val += (predicted == targets).sum().item()
                total_val += len(inputs)

        avg_loss_val = total_loss_val / total_val
        accuracy_val = correct_val / total_val

        val_losses.append(avg_loss_val)
        val_accuracies.append(accuracy_val)

        print(
            f"Epoch [{epoch+1}/{num_epochs}], Train Loss: {avg_loss_train:.4f}, Train Accuracy: {accuracy_train:.4f}, Val Loss: {avg_loss_val:.4f}, Val Accuracy: {accuracy_val:.4f}"
        )

        # Step the scheduler with the validation loss
        scheduler.step(avg_loss_val)

        # Save the model if validation loss is the lowest
        if avg_loss_val < best_val_loss:
            best_val_loss = avg_loss_val
            best_model_state = model.state_dict()
            best_epoch = epoch

        # early stopping
        early_stopping(avg_loss_val, model)
        if early_stopping.early_stop:
            print(f"[Early stop] at Epoch: {epoch}")
            break

    # Step 8: Load the best model
    if best_model_state is not None:
        model.load_state_dict(best_model_state)

    ### pytorch model
    torch.save(
        model.state_dict(),
        f"/gridgroup/cms/jxiao/hgg/dl/lmtree/HiggsDNA/tests/{model_name}.pth",
    )
    ### onnx model
    model.eval()
    # Define some dummy input and conditioning data for the model
    dummy_input = torch.randn(
        (1, _input_size + _conditioning_size), dtype=torch.float32
    )
    inputs = dummy_input
    input_names = ["feature"]
    output_names = ["output"]
    # Export the model to ONNX format
    torch.onnx.export(
        model,
        inputs,
        f"/gridgroup/cms/jxiao/hgg/dl/lmtree/HiggsDNA/tests/{model_name}.onnx",
        export_params=True,
        input_names=input_names,
        output_names=output_names,
        dynamic_axes={
            "feature": {0: "batch_size"},
            "output": {0: "batch_size"},
        },
    )

    # Plotting the training and validation losses
    plt.figure(figsize=(10, 10))
    plt.plot(train_losses, label="Training Loss")
    plt.plot(val_losses, label="Validation Loss")
    plt.xlabel("Epochs")
    plt.ylabel("Loss")
    plt.legend()
    plt.title("Training and Validation Loss over Epochs")
    plt.savefig(f"all_plots/Loss_Epochs_{model_name}.png", bbox_inches="tight")
    plt.savefig(f"all_plots/Loss_Epochs_{model_name}.pdf", bbox_inches="tight")
    plt.close()

    # Plotting the training and validation losses
    plt.figure(figsize=(10, 10))
    plt.plot(train_accuracies, label="Training Accuracy")
    plt.plot(val_accuracies, label="Validation Accuracy")
    plt.xlabel("Epochs")
    plt.ylabel("Accuracy")
    plt.legend()
    plt.title("Training and Validation Accuracy over Epochs")
    plt.savefig(f"all_plots/Accuracy_Epochs_{model_name}.png", bbox_inches="tight")
    plt.savefig(f"all_plots/Accuracy_Epochs_{model_name}.pdf", bbox_inches="tight")
    plt.close()


X_train_2 = pd.concat([X_train, mass_train], axis=1)
X_test_2 = pd.concat([X_test, mass_test], axis=1)

train_nn(
    model=NNClassifier(X_train_2.shape[1]),
    early_stopping=EarlyStopping(
        patience=10, verbose=True, path="checkpoint_NN_with_mass.pt"
    ),
    num_epochs=300,
    batch_size=128,
    model_name="nn_with_mass_epoch300",
    _input_size=X_train_2.shape[1],
    _conditioning_size=0,
    _X_train=X_train_2,
    _mass_train=None,
    _y_train=y_train,
    _final_wgt_train=final_wgt_train,
    _X_test=X_test_2,
    _mass_test=None,
    _y_test=y_test,
    _final_wgt_test=final_wgt_test,
)

import shap

model = NNClassifier(X_train_2.shape[1])
model.load_state_dict(
    torch.load(
        "/gridgroup/cms/jxiao/hgg/dl/lmtree/HiggsDNA/tests/nn_with_mass_epoch300.pth"
    )
)


# model.eval()
def predict(inputs):
    # Preprocess X if needed (e.g., convert to tensors)
    return model(torch.tensor(inputs.to_numpy(), dtype=torch.float32)).flatten()


explainer = shap.Explainer(predict, X_train_2[:100])
shap_values = explainer(X_test_2[:500])

plt.figure(figsize=(10, 10))
shap.plots.beeswarm(shap_values, max_display=20)
plt.savefig(f"all_plots/shap_nn_with_mass_epoch300.png", bbox_inches="tight")
plt.savefig(f"all_plots/shap_nn_with_mass_epoch300.pdf", bbox_inches="tight")
plt.close()

for i in [None, 65, 75, 85, 95, 105]:
    print(f"===> mass {i}")
    compare_roc(
        "nn_with_mass_epoch300",
        "/gridgroup/cms/jxiao/hgg/dl/lmtree/HiggsDNA/tests/nn_with_mass_epoch300.pth",
        ninput=X_train_2.shape[1],
        mass=i,
        X_test=X_test_2,
        y_test=y_test,
        mass_test=mass_test,
        X_train=X_train_2,
        y_train=y_train,
        mass_train=mass_train,
    )
    compare_score(
        "nn_with_mass_epoch300",
        "/gridgroup/cms/jxiao/hgg/dl/lmtree/HiggsDNA/tests/nn_with_mass_epoch300.pth",
        ninput=X_train_2.shape[1],
        mass=i,
        X_test=X_test_2,
        y_test=y_test,
        mass_test=mass_test,
        X_train=X_train_2,
        y_train=y_train,
        mass_train=mass_train,
    )
for i in [60, 70, 80, 90, 100, 110]:
    print(f"===> mass {i}")
    compare_roc(
        "nn_with_mass_epoch300",
        "/gridgroup/cms/jxiao/hgg/dl/lmtree/HiggsDNA/tests/nn_with_mass_epoch300.pth",
        ninput=X_train_2.shape[1],
        mass=i,
        X_test=pd.concat([X2_test, mass2_test], axis=1),
        y_test=y2_test,
        mass_test=mass2_test,
    )
    compare_score(
        "nn_with_mass_epoch300",
        "/gridgroup/cms/jxiao/hgg/dl/lmtree/HiggsDNA/tests/nn_with_mass_epoch300.pth",
        ninput=X_train_2.shape[1],
        mass=i,
        X_test=pd.concat([X2_test, mass2_test], axis=1),
        y_test=y2_test,
        mass_test=mass2_test,
    )

train_nn(
    model=NNClassifier(X_train.shape[1]),
    early_stopping=EarlyStopping(
        patience=10, verbose=True, path="checkpoint_NN_without_mass.pt"
    ),
    num_epochs=300,
    batch_size=128,
    model_name="nn_without_mass_epoch300",
    _input_size=X_train.shape[1],
    _conditioning_size=0,
    _X_train=X_train,
    _mass_train=None,
    _y_train=y_train,
    _final_wgt_train=final_wgt_train,
    _X_test=X_test,
    _mass_test=None,
    _y_test=y_test,
    _final_wgt_test=final_wgt_test,
)

import shap

model = NNClassifier(X_train.shape[1])
model.load_state_dict(
    torch.load(
        "/gridgroup/cms/jxiao/hgg/dl/lmtree/HiggsDNA/tests/nn_without_mass_epoch300.pth"
    )
)


# model.eval()
def predict(inputs):
    # Preprocess X if needed (e.g., convert to tensors)
    return model(torch.tensor(inputs.to_numpy(), dtype=torch.float32)).flatten()


explainer = shap.Explainer(predict, X_train[:100])
shap_values = explainer(X_test[:500])

plt.figure(figsize=(10, 10))
shap.plots.beeswarm(shap_values, max_display=20)
plt.savefig(f"all_plots/shap_nn_without_mass_epoch300.png", bbox_inches="tight")
plt.savefig(f"all_plots/shap_nn_without_mass_epoch300.pdf", bbox_inches="tight")
plt.close()

for i in [None, 65, 75, 85, 95, 105]:
    print(f"===> mass {i}")
    compare_roc(
        "nn_without_mass_epoch300",
        "/gridgroup/cms/jxiao/hgg/dl/lmtree/HiggsDNA/tests/nn_without_mass_epoch300.pth",
        ninput=X_train.shape[1],
        mass=i,
        X_test=X_test,
        y_test=y_test,
        mass_test=mass_test,
        X_train=X_train,
        y_train=y_train,
        mass_train=mass_train,
    )
    compare_score(
        "nn_without_mass_epoch300",
        "/gridgroup/cms/jxiao/hgg/dl/lmtree/HiggsDNA/tests/nn_without_mass_epoch300.pth",
        ninput=X_train.shape[1],
        mass=i,
        X_test=X_test,
        y_test=y_test,
        mass_test=mass_test,
        X_train=X_train,
        y_train=y_train,
        mass_train=mass_train,
    )
for i in [60, 70, 80, 90, 100, 110]:
    print(f"===> mass {i}")
    compare_roc(
        "nn_without_mass_epoch300",
        "/gridgroup/cms/jxiao/hgg/dl/lmtree/HiggsDNA/tests/nn_without_mass_epoch300.pth",
        ninput=X_train.shape[1],
        mass=i,
        X_test=X2_test,
        y_test=y2_test,
        mass_test=mass2_test,
    )
    compare_score(
        "nn_without_mass_epoch300",
        "/gridgroup/cms/jxiao/hgg/dl/lmtree/HiggsDNA/tests/nn_without_mass_epoch300.pth",
        ninput=X_train.shape[1],
        mass=i,
        X_test=X2_test,
        y_test=y2_test,
        mass_test=mass2_test,
    )
